<?php
class ImagineClient
{
    /**
     * Статическая переменная, в которой мы
     * будем хранить экземпляр класса
     *
     * @var SingletonTest
     */
    protected static $_instance;
    private $host;
    private $port;
 
    /**
     * Закрываем доступ к функции вне класса.
     * Паттерн Singleton не допускает вызов
     * этой функции вне класса
     *
     */
    private function __construct($host,$port){
        $this->host = $host;
        $this->port = $port;
        /**
         * При этом в функцию можно вписать
         * свой код инициализации. Также можно
         * использовать деструктор класса.
         * Эти функции работают по прежднему,
         * только не доступны вне класса
         */
    }
 
    /**
     * Закрываем доступ к функции вне класса.
     * Паттерн Singleton не допускает вызов
     * этой функции вне класса
     *
     */
    private function __clone(){
    }
    /**
     * Статическая функция, которая возвращает
     * экземпляр класса или создает новый при
     * необходимости
     *
     * @return SingletonTest
     */
    public static function getInstance($host='localhost',$port=4046) {
        // проверяем актуальность экземпляра
        if (null === self::$_instance) {
            // создаем новый экземпляр
            self::$_instance = new self($host,$port);
        }
        // возвращаем созданный или существующий экземпляр
        return self::$_instance;
    }
    public function upload($url,$isUrl=false) {
        if (!$isUrl){
            $file_name_with_full_path = realpath($url);
            $post = array('file'=>'@'.$file_name_with_full_path);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,$this->host.':'.$this->port.'/upload');
            curl_setopt($ch, CURLOPT_POST,1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        } else {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,$this->host.':'.$this->port.'/upload?url='.$url);
        }
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result=json_decode(curl_exec ($ch),true);
        curl_close ($ch);
        return $result;
    }
    public function resize($filepath,$w,$h) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$this->host.':'.$this->port.'/resize?h='.$h.'&w='.$w.'&id='.$id);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
        $result=json_decode(curl_exec ($ch),true);
        curl_close ($ch);
        return $result;
    }
    public function crop($filepath,$w,$h,$side=false){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$this->host.':'.$this->port.'/resize?h='.$h.'&w='.$w.'&id='.$id.($side?'&side='.$side:''));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
        $result=json_decode(curl_exec ($ch),true);
        curl_close ($ch);
        return $result; 
    }
    public function delete($filepath){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$this->host.':'.$this->port.'/delete/id'.$id);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
        $result=json_decode(curl_exec ($ch),true);
        curl_close ($ch);
        return $result; 
    }
    public function update($filepath){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$this->host.':'.$this->port.'/update/id'.$id);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
        $result=json_decode(curl_exec ($ch),true);
        curl_close ($ch);
        return $result; 
    }
}
?>